
function Restaraunt(name, rating, lastVisit){
    this.name = name;
    this.rating = rating;
    this.lastVisit = lastVisit;
}


let resArr = [
    new Restaraunt('Mongolian place', 5, new Date(2018, 10, 9)), 
    new Restaraunt('Sweeto Burrito', 5, new Date(2018, 10, 16)), 
    new Restaraunt('Tandoori Oven', 4, new Date(2018, 10, 2)), 
    new Restaraunt('Panda Express', 5, new Date(2018, 10, 2)), 
    new Restaraunt('Chili\'s', 5, new Date(2018, 10, 2)), 
    new Restaraunt('The Crepery', 3, new Date(2018, 10, 2)), 
    new Restaraunt('Cafe Sabor', 2, new Date(2018, 10, 2)), 
    new Restaraunt('Takara Sushi', 4, new Date(2018, 10, 2)), 
    new Restaraunt('Noodles and Co', 1, new Date(2018, 10, 2)), 
    new Restaraunt('Olive Garden', 1, new Date(2018, 10, 2)), 
    new Restaraunt('Texas Roadhouse', 1, new Date(2018, 10, 2)), 
    new Restaraunt('Buffalo Wild Wings', 1, new Date(2018, 10, 2)), 
    new Restaraunt('Little Caesars', 1, new Date(2018, 10, 2)), 
    new Restaraunt('Costa Vida', 1, new Date(2018, 10, 2)), 
    new Restaraunt('Jamba Juice', 1, new Date(2018, 10, 2)), 
    new Restaraunt('Einstein Bagels', 1, new Date(2018, 10, 2)), 
    new Restaraunt('Firehouse Pizza', 1, new Date(2018, 10, 2)), 
    new Restaraunt('Arctic Circle', 1, new Date(2018, 10, 2)), 
    new Restaraunt('Wendy\'s', 1, new Date(2018, 10, 2)), 
    new Restaraunt('Aggie Creamery', 2, new Date(2018, 10, 2)), 
];

let totalVal = 0;
let valArr = [];
for(var i = 0; i < resArr.length; i++){
    let days = Math.floor(new Date() / 86400000) - Math.floor(resArr[i].lastVisit / 86400000);
    totalVal += days * resArr[i].rating;
    valArr.push(resArr[i].rating * days);
}

function doOne(){
    let rand = Math.floor(Math.random() * totalVal + 1);
    let final = "";
    for(let i = 0; i < resArr.length; i++){
        rand -= valArr[i];
        if(rand < 0){
            final = resArr[i].name;
            break;
        }
    }
    return final;
}

function pick(){
    document.getElementById('which').textContent = doOne();
}

function pick3(){
    document.getElementById('which').innerHTML = 
        '<ol><li>' + doOne() + '</li><li>' + doOne() + '</li><li>' + doOne() + '</li></ol>';
}

document.getElementById('pick').onclick = function(){ pick(); };
document.getElementById('pick3').onclick = function(){ pick3(); };
